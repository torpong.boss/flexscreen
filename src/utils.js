export const IsFibanacci = (num) => {
  if (IsPerfectSquare(5*(num*num)-4) || IsPerfectSquare(5*(num*num)+4)) {
     return true;
  } else { return false; }
}

export const IsPrime = (num) => {
  for(let i = 2, s = Math.sqrt(num); i <= s; i++)
    if(num % i === 0) return false; 
  return num > 1;
}

export const IsPerfectSquare =(x) => {
    let s = parseInt(Math.sqrt(x));
    return (s * s === x);
}
