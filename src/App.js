import './App.css';
import FlexScreen from './FlexScreen';
import styled from "styled-components";

function App() {
  return (
    <AppDiv className="App">
      <FlexScreen></FlexScreen>
    </AppDiv>
  );
}

export default App;

const AppDiv = styled.div`
  height: 100vh;
  width: 100vw;
`;
