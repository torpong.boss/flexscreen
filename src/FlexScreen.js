import React, { useState } from "react";
import styled from "styled-components";
import {IsFibanacci, IsPrime} from "./utils"

const FlexScreen = props => {

  const [inputNumber, setNumber] = useState(0)
  const [method, setMethod] = useState("isPrime")
  const [result, setResult] = useState("")

  const handleKeyPress = (event) => {
    if(event.key === "Enter"){
      if (Math.sign(event.target.value) === -1){
        event.target.value = -1
        setNumber(-1)
        
      }else{
        event.target.value = Math.round(event.target.value)
        setNumber(Math.round(event.target.value))
      }

      if (method === "isPrime"){
        setResult(IsPrime(inputNumber))
      }else{
        setResult(IsFibanacci(inputNumber))
      }
    }
  }

  return (
    <MainDiv>
      <InputDiv>
        <input type="number" id="one"  onKeyPress={handleKeyPress} />
      </InputDiv>
      <ChooseCalDiv>
        <select name="method" id="two" defaultValue="isPrime" onChange={(selected) => setMethod(selected.target.value)}>
          <option value="isPrime">isPrime</option>
          <option value="IsFibanacci">IsFibanacci</option>
        </select>
      </ChooseCalDiv>
      <ResultDiv>
        <p>{result.toString()}</p>
      </ResultDiv>
    </MainDiv>
  );
};

export default FlexScreen;


const MainDiv = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: space-between;
  min-width: 600px;
`

const InputDiv = styled.div`
  flex-basis: 200px;
  background-color: red;
  height: 100%;
`;

const ChooseCalDiv = styled.div`
  flex-basis: 100px;
  flex-grow: 1;
  background-color: blue;
  height: 100%;
`;

const ResultDiv = styled.div`
  flex-basis: 300px;
  background-color: yellow;
  height: 100%;
`